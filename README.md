# Car Price Prediction – Random Forest – End to End with Deployment

•	The dataset contains the information about the used cars with variables like year, present price, selling price, kilometers driven, fuel type, seller type, transmission type and number of owners. The data contains numerical and categorical variables with no missing or null values.

•	The variable like ‘Car_Name’ is dropped because it serves no purpose in predicting the selling price for the car. The variable named ‘Year’ is replaced with the variable ‘no_year’ indicating the number of years from the reference point 2020.

•	The categorical variables like ‘Fuel_Type’, ‘Seller_Type’ and ‘Transmission’ are converted into the dummy variables as they are important in modelling the selling price of cars.

•	The pairplot and heatmaps are used to investigate the collinearity among the predictor variables, using the seaborn python library. The ExtraTreesRegressor model is used to understand the feature importance’s of the predictor variables. The variable ‘Present_Price’ has significantly very high feature importance.

•	The data is divided into train and test splits with 85-15% proportions.

•	6 different machine learning regression models like Linear Regression, SVR, LinearSVR, Ridge Regression, SGDRegressor, RandomForestRegressor are fitted to the training data. Different models are compared based on the R2 value on the training data.

•	It was found out that the random forest regressor achieved the best R2 value and linear regression achieved overfitting, so that’s why not considered for the possible final model.

•	The hyperparameters like ‘criterion’, ‘min_samples_leaf’, ‘min_samples_split’ and ‘n_estimators’ are tuned using the GridSearchCV.

•	The pickle version of the final tuned model was used to deploy the project through HTML using the flask python library.

***Techniques Used:*** LinearRegression, SVR, Ridge, LinearSVR, SGDRegressor, RandomForestRegressor, GridSearchCV, seaborn, sklearn, flask, pickle. (**Python**)

